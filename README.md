1) garanta que o computador possui o JDK versão 1.8, Maven versão 3.6.0, MongoDB versão 4.0.4 e git (qualquer versão recente) instalados. Para isso, abra uma janela de seu terminal e execute os comandos 

```
java -version
mvn -version
mongod -version
git --version
```

As versões de cada um devem ser impressas na saída padrão.

2) clone este repositório para o seu computador

3) inicie o MongoDB executando o comando `mongod`. Isso impedirá você de utilizar esta janela do terminal, portanto abra uma nova e passe a usá-la, mas deixe a anterior aberta

3) utilizando a nova janela do terminal, navegue até o diretório local onde você clonou este repositório 

4) entre no o diretório do microserviço de autenticação, ou `./auth_microservice` 

5) execute o comando `mvn spring-boot:run` 

6) abra uma terceira janela de seu terminal e, novamente, navegue até o diretório local onde se encontra este repositório 

7) entre no diretório do microserviço de lançamentos contábeis, ou `./accounting_microservice`, e finalmente, execute o comando `mvn spring-boot:run` 

8) abra uma quarta janela de seu terminal e, novamente, navegue até o diretório local onde se encontra este repositório >

9) entre no diretório do microserviço de gastos, ou `./spend_microservice`, e finalmente, execute o comando `mvn spring-boot:run` 

10) Logo, você estará pronto para abrir o POSTMAN e testar os ENDPOINTS! 



#### Observações
- Os microserviços estão configurados para ocuparem as portas 8080 , 8081 e 8082  respectivamente.
- Para executar os testes unitários do microserviço de gastos, é preciso que o microserviço de autenticação esteja online
- Este repositório conta com uma collection e um environment do Postman, uma ferramenta muito útil para testar as funcionalidades de APIs. Para utilizá-los, faça o download e instale o Postman em seu computador (caso ainda não o possua intalado) e importe tanto collection quanto environment para o seu ambiente.




#### Possíveis otimizações (requisitos não funcionais)
1) Para melhorar a velocidade de resposta da rota de inserção de lançamentos, seria interessante refatorá-la para que ela inserisse os documentos a serem persistidos primeiramente em um sistema de mensageria, ou no próprio Redis (devido a sua velocidade de inserção e consulta). Após isso, workers assíncronos subscritos ao canal de mensagens seriam responsáveis por persistir os dados no banco de dados de fato

2) Containerizar cada um dos microserviços tornaria muito mais simples a tarefa de movê-los de um infraestrutura para outra, escalá-los e administrá-los no geral

