package microservice.controller_tests;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import javax.servlet.http.HttpServletRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.net.URI;
import java.text.SimpleDateFormat;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import microservice.controllers.AccountingController;
import microservice.models.Authorization;
import microservice.models.Entry;
import microservice.util.AuthRequester;
import org.springframework.web.util.UriComponentsBuilder;
import java.math.BigDecimal;


@RunWith(SpringRunner.class)
@WebMvcTest(value = AccountingController.class, secure = false)
public class AccountingControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
    private AccountingController controller;
    
    private final ObjectMapper mapper = new ObjectMapper();
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
    
    @Test
	public void testInsertSpendJSONResponse() throws Exception {
        String mockDateStr = "2019-09-03T02:32:13.743+0000";
        Date mockDate = dateFormat.parse(mockDateStr);

        Entry mockEntry = new Entry();
        mockEntry.set_id("4c32112ea827172b1d2a12sz");
        mockEntry.setDescription("dummyDescription");
        mockEntry.setValor(new BigDecimal(123.456));
        mockEntry.setData(mockDate);

        ResponseEntity<Entry> mockCreatedResponse = ResponseEntity.created(new URI("http://localhost:8082/account/4c32112ea827172b1d2a12sz")).body(mockEntry);

		when(controller.register(any(UriComponentsBuilder.class), any(Entry.class))).thenReturn(mockCreatedResponse);
        
        Authorization authorization = AuthRequester.authenticate(
			"http://localhost:8080/systems/authentication", 
			"mySystem", 
            "54321naz");
            
        String expected = mapper.writeValueAsString(mockEntry);

        String content = "{\"description\":\"dummyDescription\",\"value\":123.456,\"date\":\"2018-12-20T02:32:13.743+0000\"}";

        this.mockMvc.perform(
            post("/accounting")
                .header("Authorization", authorization.getAccessToken())
                .content(content)
                .contentType(MediaType.APPLICATION_JSON))
			.andDo(print())
            .andExpect(status().isCreated())
            .andExpect(content().json(expected))
            .andExpect(content().string(expected));

	}

    @Test
	public void testListUserEntriesJSONResponse() throws Exception {
        String mockDateStr = "2018-12-18T02:48:26.163+0000";
        String startDateStr = "2018-12-15T02:48:26.163+0000";
        String endDateStr = "2018-12-20T21:51:33.775+0000";

        Date mockDate = dateFormat.parse(mockDateStr);

        Entry firstEntry = new Entry("5c1afa52dd3b7e2268264e9d", "dummyDescription1", new BigDecimal(231.465), mockDate);
        Entry secondEntry = new Entry("5c1afbb5dd3b7e2268264ead", "dummyDescription2", new BigDecimal(1910.1011), mockDate);
        Entry thirdEntry = new Entry("5c1aff16dd3b7e2698e06a1e", "dummyDescription3", new BigDecimal(3212.1415), mockDate);

		List<Entry> mockSpends = Arrays.asList(firstEntry, secondEntry, thirdEntry);

        when(controller.getUserSpends(any(HttpServletRequest.class), anyString(), anyString()))
        .thenReturn(ResponseEntity.ok(mockSpends));
        
        Authorization authorization = AuthRequester.authenticate(
			"http://localhost:8080/users/authentication", 
			"marcalcantara", 
			"timao1910");

		String expected = mapper.writeValueAsString(mockSpends);

        this.mockMvc.perform(
            get("/lancamentos-contabeis")
                .param("start_date", startDateStr)
                .param("end_date", endDateStr)
				.header("Authorization", authorization.getAccessToken()))
			.andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().json(expected))
            .andExpect(content().string(expected));
   
	}
}
