package microservice.services;


import microservice.repositories.AccountingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import microservice.models.AccountingSummary;
import microservice.models.Entry;
import org.bson.types.ObjectId;
import org.modelmapper.internal.bytebuddy.build.Plugin.Engine.Summary;

import microservice.models.Message;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import javax.xml.bind.ValidationException;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Calendar;
import java.text.SimpleDateFormat;
import java.util.TimeZone;
import java.text.ParseException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;


@Service
public class EntryService {
    
    @Autowired
    private AccountingRepository accRepo;
    
    @Async("ThreadPoolExecutor")
    public CompletableFuture<Entry> insert(Entry entry) {
        return CompletableFuture.completedFuture(accRepo.save(entry));  
    }

    @Async("ThreadPoolExecutor")
    public CompletableFuture<List<Entry>> filterByAccount(String account){
    	return CompletableFuture.completedFuture(accRepo.findByAccount(account) );
    }

    @Async("ThreadPoolExecutor")
    public CompletableFuture<?> filterBetweenDates(String startDateStr, String endDateStr, String userId)
        throws ParseException, ValidationException {
        
        startDateStr = startDateStr.replace("Z", "+0000").replace(" ", "+");
        endDateStr = endDateStr.replace("Z", "+0000").replace(" ", "+");
        
        // getting the current Date as a UTC aware object
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        Date now = calendar.getTime();
        
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        Date startDate = startDateStr.trim().isEmpty() ? addDays(now, -7) : dateFormat.parse(startDateStr);
        Date endDate = startDateStr.trim().isEmpty() ? now : dateFormat.parse(endDateStr);

        if (startDate.after(endDate)) 
            return CompletableFuture.completedFuture(new Message("startDate cannot be after endDate", userId, "failed"));
        else if (addDays(startDate, 21).before(endDate)) 
            return CompletableFuture.completedFuture(new Message("the time range between startDate and endDate have to be smaller than 21 days", userId, "failed"));
     
        return CompletableFuture.completedFuture(accRepo.findByStartAndEndDate(startDate, endDate, userId));  
    }


    public static Date addDays(Date date, int days){
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        cal.setTime(date);
        cal.add(Calendar.DATE, days); //minus number would decrement the days
        return cal.getTime();
    }


	public CompletableFuture<Optional<Entry>> filterById(String id) throws InterruptedException, ExecutionException {
		
		CompletableFuture<Optional<Entry>> entriesFuture = CompletableFuture.completedFuture(accRepo.findById(id) );
            return entriesFuture;
		
	}

	
}