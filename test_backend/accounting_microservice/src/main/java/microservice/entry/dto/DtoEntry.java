package microservice.entry.dto;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DtoEntry {
	public String contaContabil;
	public String data;
	public BigDecimal valor;
	
	
	public void createDto(String contaContabil, Date data, BigDecimal valor) {
		
		String pattern = "yyyyMMdd";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		
		this.contaContabil = contaContabil;
		this.data = simpleDateFormat.format(data);
		this.valor = valor;
	}
	
	
}
