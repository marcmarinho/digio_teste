package microservice.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import javax.validation.Valid;
import javax.xml.bind.ValidationException;
import microservice.models.Message;
import microservice.entry.dto.DtoEntry;
import microservice.models.AccountingSummary;
import microservice.models.Entry;
import org.springframework.http.ResponseEntity;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import org.springframework.web.util.UriComponentsBuilder;
import microservice.services.EntryService;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@RestController
public class AccountingController {

	@Autowired
	private EntryService entryService;

	@Autowired
	private ModelMapper modelMapper;

	private DtoEntry convertToDto(Entry entry) {
		DtoEntry postDto = modelMapper.map(entry, DtoEntry.class);
		postDto.createDto(entry.getContaContabil(), entry.getData(), entry.getValor());
		return postDto;
	}

	@RequestMapping(value = "/lancamentos-contabeis", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Entry> register(UriComponentsBuilder builder, @Valid @RequestBody Entry entry)
			throws URISyntaxException, InterruptedException, ExecutionException {

		CompletableFuture<Entry> accountingFuture = entryService.insert(entry);
		Entry storedEntry = accountingFuture.get();

		return ResponseEntity.created(new URI(builder.toUriString() + "/lancamentos-contabeis/" + entry.get_id()))
				.body(storedEntry);

	}

	@RequestMapping(value = "/lancamentos-contabeis/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Object> accountingEntriesByID(UriComponentsBuilder builder, @PathVariable String id)
			throws ValidationException, InterruptedException, ExecutionException, ParseException {

		CompletableFuture<Optional<Entry>> accountingEntries = entryService.filterById(id);
		Optional<Entry> result = accountingEntries.get();
		
		if(result.isPresent())
			return ResponseEntity.ok(convertToDto(result.get()));
		else
			return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
		
		}


	@RequestMapping(value = "/lancamentos-contabeis/", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Object> getAccountingEntryByID(@RequestParam Map<String, List<String>> params)
			throws ValidationException, InterruptedException, ExecutionException, ParseException {

		CompletableFuture<List<Entry>> accountingFuture = entryService
				.filterByAccount(params.keySet().iterator().next());

		List<Entry> result = accountingFuture.get();

		return ResponseEntity.ok(result.stream().map(entry -> convertToDto(entry)).collect(Collectors.toList()));

	}
	
	@RequestMapping(value = "/users/accounting", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Object> getUserSpends(HttpServletRequest request,
			@RequestParam(value = "start_date", defaultValue = "") String startDateStr,
			@RequestParam(value = "end_date", defaultValue = "") String endDateStr)
			throws ValidationException, InterruptedException, ExecutionException, ParseException {

		CompletableFuture<?> accountingFuture = entryService.filterBetweenDates(startDateStr, endDateStr,
				request.getAttribute("userId").toString());
		Object result = accountingFuture.get();
		if (result.getClass() == Message.class)
			return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
		else
			return ResponseEntity.ok(result);
	}
	
	
	/*
	 * 
	@RequestMapping(value = "/lancamentos-contabeis/stats/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Object> accountingStatsByID(UriComponentsBuilder builder, @PathVariable String id)
			throws ValidationException, InterruptedException, ExecutionException, ParseException {

		CompletableFuture<Optional<Entry>> accountingEntries = entryService.filterById(id);
		Optional<Entry> result = accountingEntries.get();
		
		if(result.isPresent())
			return ResponseEntity.ok(convertToDto(result.get()));
		else
			return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
		
		}


	@RequestMapping(value = "/lancamentos-contabeis/stats", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Object> getAccountingStatsByID(@RequestParam Map<String, List<String>> params)
			throws ValidationException, InterruptedException, ExecutionException, ParseException {

		CompletableFuture<List<AccountingSummary>> accountingFuture = entryService
				.getTotal();

		List<AccountingSummary> result = accountingFuture.get();

		return ResponseEntity.ok(result);

	}
	 * 
	 */


}
