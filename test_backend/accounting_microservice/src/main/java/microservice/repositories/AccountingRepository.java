package microservice.repositories;


import java.util.Date;
import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import microservice.models.Entry;


public interface AccountingRepository extends MongoRepository<Entry, String> {
    
    @Query("{ 'date': { '$gte': ?0, '$lte': ?1 }, 'userCode': ?2 }")
    public List<Entry> findByStartAndEndDate(Date startDate, Date endDate, String userCode);
    
    @Query("{ 'contaContabil': ?0 }")
    public List<Entry> findByAccount(String account);

    public Entry findBy_id(String _id);
    
    

}
