package microservice.models;

import java.util.Date;
import javax.validation.constraints.NotNull;
import org.springframework.data.annotation.Id;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.math.BigDecimal;
import org.springframework.data.mongodb.core.mapping.Document;


@Document(collection = "entries")
public class Entry {
    @Id
    private String _id;
    
    @NotNull
    private String contaContabil;
    
    private String description;
    
    @NotNull
    private BigDecimal valor;

    @NotNull
    @JsonFormat(shape=JsonFormat.Shape.STRING, 
                pattern="yyyy-MM-dd'T'HH:mm:ss.SSSZ", 
                timezone="UTC")
    private Date data;

    public Entry() { }

    public Entry(Entry otherSpend) { 
        this.description = otherSpend.getDescription();
        this.valor = otherSpend.getValor();
        this.data = otherSpend.getData();
    }

    public Entry(String _id, 
                 String description, 
                 BigDecimal valor, 
                 Date date) {
        this._id = _id;
        this.description = description;
        this.valor = valor;
        this.data = date;
    }

    public String get_id() { return _id; }

    public String getDescription() { return description; }

    public BigDecimal getValor() { return valor; }

    public Date getData() { return data; }

    public void set_id(String _id) { this._id = _id; }

    public void setDescription(String description) { this.description = description; }

    public void setValor(BigDecimal valor) { this.valor = valor.setScale(2, BigDecimal.ROUND_HALF_UP); }

    public void setData(Date date) { this.data = date; }

	public String getContaContabil() {
		return contaContabil;
	}

	public void setContaContabil(String contaContabil) {
		this.contaContabil = contaContabil;
	}
	
	

}